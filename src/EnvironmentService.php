<?php

declare(strict_types=1);

/*
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 */

namespace Reelworx\TYPO3\FakeFrontend;

/**
 * This class is used to create an extbase UriBuilder, which is constantly in Frontend Mode.
 * Only needed in Core v10
 */
class EnvironmentService extends \TYPO3\CMS\Extbase\Service\EnvironmentService
{
    public function isEnvironmentInBackendMode(): bool
    {
        return false;
    }
}
