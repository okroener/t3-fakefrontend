# TYPO3 Fake Frontend

This library for TYPO3 extensions provides a utility to bootstrap a fake frontend
environment (TSFE and other things) to be used when doing frontend related stuff
from within other contexts like the backend or CLI.

## When to use this library?

This library comes in handy if you need to generate absolute frontend links for site
from within non-frontend context.

Examples:
* If you generate mails from within a scheduler task, which should include links to frontend.
  You can the usual Fluid viewhelpers, once the "fake frontend" is established.
* If you generate frontend links in a backend module.

## Usage

```php
// Note: This populates the globals TSFE, TYPO3_REQUEST and $_SERVER['HTTP_HOST']
// It is advisable to reset/unset those again, when not needed anymore.
// Especially in BE context this can cause unexpected side effects. 

// create a fully initialized TSFE for given page uid
$requestBackup = $GLOBALS['TYPO3_REQUEST'] ?? null;
$valid = \Reelworx\TYPO3\FakeFrontend\FrontendUtility::buildFakeFE(<pageUid>);
if ($valid) {
    // ... do the work
    if ($requestBackup) {
        $GLOBALS['TYPO3_REQUEST'] = $requestBackup;
    }
    unset($GLOBALS['TSFE']);
} else {
    // If !$valid, check FrontendUtility::$lastError for details
}
```

A specific `$GLOBALS['TSFE']->absRefPrefix` can be enforced by configuring `config.cliDomain` in the template of the page.
The value `auto` means to use `GeneralUtility::getIndpEnv('TYPO3_SITE_PATH')` as absRefPrefix.
